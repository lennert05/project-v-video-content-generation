<div align="center">
  <h1><strong>Project V - Video Content Generation </strong></h1>
  <p>
    <strong>Een programma om een video te maken uit een beurs rapport</strong>
  </p>
  ![Video](tesla-text-to-speech.mp4)
</div>

# Inhoudstafel
- [Wat?](#wat-hebben-we-gemaakt)
- [Dependencies](#dependencies)
- [Installatie](#installatie)
- [Configuratie](#configuratie)
- [Text to speech](#text-to-speech)
- [Uitles code](#uitleg-code)
- [Auteurs](#auteurs)


# Wat hebben we gemaakt
De video content generator is een POC geschreven in java met een externe renderer die geschreven is in typescript/javascript.
De flow die het programma doorloopt is het ophalen van de nodige data over een rapport van [chartmill](https://www.chartmill.com/home) waaronder de afbeeldingen en de tekst voor in de video.
Daarna wordt er een tijdlijnfile opgesteld met de locatie van de nodige files, de nodige tekst en dan de duration in. Het formaat van deze file is JSON.

Wat is dit niet:

> Een api die constant request kan ontvangen en constant aan het draaien is.<br>
> Een volledig afgewerkt programma die alles op productie level kan verwerken, het is een POC.

Wat het wel kan:

> Met de juist opgegeven endpoints in de configuratie file een video genereren!

# Dependencies

## Java libraries (terug te vinden in build.gradle)

- [jaffree](https://github.com/kokorin/Jaffree) 
- [vertx](https://vertx.io/) 
- [jackson](https://github.com/FasterXML/jackson) 
- [gson](https://github.com/google/gson) 
- [selenium](https://github.com/seleniumhq/selenium) 
- [common-io](https://commons.apache.org/proper/commons-io/) 
- [json](https://github.com/stleary/JSON-java) 
- [aws-java-sdk-polly](https://docs.aws.amazon.com/sdk-for-java/index.html) 
- [jlayer](https://mvnrepository.com/artifact/com.googlecode.soundlibs/jlayer/1.0.1.4) 

## Andere dependencies

- [ffmpeg](https://ffmpeg.org/) 
- onze remotion renderer
- Firefox [geckodriver](https://www.selenium.dev/documentation/webdriver/getting_started/install_drivers/)

# Installatie

1. Clone of download onze repository en ga in die folder.
```bash
git clone https://git.ti.howest.be/se-project/project-v-video-content-generation.git
cd project-v-video-content-generation
```
2. Voor het moment hebben we gemerkt als je het programma runt vanuit Intellij, maar als je de jar build kan het programma niet communiceren met de geckodriver voor het runnen van een headless firefox.
```bash
gradle shadowJar
```
3. Kopieer de jar naar de map die je wilt
```bash
cp build/libs/videobeurzen-java-1.0-POC-all.jar <gewenste directory>
```
4. De eerste keer dat je het gebruikt, zal het niet werken, maar wel een config file gegenereerd hebben in de map waar de jar zich bevindt. Meer hierover in de [configuratie](#configuratie).
(hij zal helaas crashen met de error dat hij geen executable driver heeft. Dit zou de nodige documentatie moeten zijn maar bij ons lukte dit niet om te runnen in een jar: [documentatie](https://www.selenium.dev/documentation/webdriver/getting_started/install_drivers/))

# Configuratie

De config file die gegenereerd wordt is een kopie van de default-config.json die zich bevindt in de resources van de applicatie.

| Key                  | Beschrijving                                                                                                                                                                                  | voorbeeld                                                                                                                                                                     |
|----------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| host                 | De url van het rapport                                                                                                                                                                         | https://www.chartmill.com/stock/quote/TSLA/fundamental-analysis                                                                                                               |
| apiHost              | De url van de api                                                                                                                                                                             | https://chartmill.com                                                                                                                                                         |
| requestURI           | Het pad van de resource die wordt gevraagd van de api                                                                                                                                        | "/chartmill-rest/analyze/fa/11711"                                                                                                                                            |
| port                 | Poort waarop de api beschikbaar is                                                                                                                                                            | 433                                                                                                                                                                           |
| cookies              | De cookies die meegegeven moeten worden voor authenticatie                                                                                                                                    | <pre lang="json">{   <br>    "_ga": "",<br>    "_gid": "",<br>    "sessionId": "",<br>    "o": "",<br>    "viewAmount": "",<br>    "_gat": "",<br>    "__gads": ""<br>}</pre> |
| localStorage         | De items die in de localstorage aanwezig moeten zijn om het cookie venster te verbergen voor de screenshots                                                                                   | <pre lang="json">{<br>    "consentDate": "",<br>    "consentGiven": "[]"<br>}</pre>                                                                                           |
| xpathLocations       | Het xpath van de web elementen waar je een screenshot wilt van nemen (in dit senario zouder er altijd 6 moeten zijn)                                                                           | <pre lang="json">{<br>    "faRating": "/html/body/...",<br>    ...<br>}</pre>                                                                                                 |
| driverName           | Niet veranderen tenzij er een andere webdriver nodig is door een specifiek systeem (diver voor linux gebruikt voor de default), driver die wordt gebruikt voor headless firefox               | webdriver.gecko.driver                                                                                                                                                        |
| driverPath           | Niet veranderen tenzij er een andere webdriver nodig is voor een specifiek systeem (diver voor linux gebruikt voor de default), de locatie van de driver in de resources folder van de driver | /geckodriver                                                                                                                                                                  |
| workFolder           | Is de folder waar de images en audio folder zich in bevinden                                                                                                                                  | ./                                                                                                                                                                            |
| imageFolder          | De folder waar de images in geplaatst worden voor de video                                                                                                                                    | ./images/                                                                                                                                                                     |
| audioFolder          | De folder waar de audio in wordt geplaatst voor de video                                                                                                                                      | ./audio/                                                                                                                                                                      |
| jsVideoBuilderFolder | De folder van de remotion renderer                                                                                                                                                            | /home/videobuilder/                                                                                                                                                           |

# Text to speech
Voor onze text-to-speech maken we gebruik van AWS Polly. Om dit werkend te krijgen, moeten wel enkele stappen ondernomen worden anders zal het programma een credentials error geven voor Polly.

Maak [hier](https://aws.amazon.com) een AWS account aan. Om Polly te gebruiken moet je je kredietkaart verifiëren, dit zal 1 dollar kosten ofwel 89 cent.

To use Polly, we need an acces token and a secret token. Unfortanetely, this isn't possible on the root account. We will have to create a new group with administrator access and a new user.

Ga naar de [IAM Console](https://console.aws.amazon.com/iamv2/home?#/home). 
1. Ga naar users, klik op "add user". Vink access token aan, en geef de user een wachtwoord.
2. Ga naar user groups, maak een group aan met de naam "Administrator" en geef het de rechten "AdministratorAccess".
3. Voeg de user toe aan de user group.

Nu gaan we een access key en secret key aanmaken.
1. Ga naar users in de IAM Console.
2. Ga naar de Security Credentials tab.
3. Druk op "create access key".
4. Bewaar de secret key, hier kan je achteraf niet meer aan. De access key kan je altijd raadplegen.

Nu gaan we AWS CLI installeren, om onze AWS credentials er in te geven zodat we gebruik kunnen maken van Polly.
```
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
```

Laten we onze AWS credentials registreren.

1. Ga naar de aws folder
```
cd ~/.aws
```
2. Pas de credentials file aan naar onderstaande content
```
[default]
aws_access_key_id = ACCES_KEY
aws_secret_access_key = SECRET_KEY
```
3. Pas de config file aan naar onderstaande content
```
[default]
region = us-west-2
output=json
```

Nu is alles klaar om AWS Polly te gebruiken. Voor meer informatie raadpleeg de [Polly documentatie](https://docs.aws.amazon.com/polly/latest/dg/what-is.html)


# Uitleg code
## Clients
Onder de clients map is de stockClient te vinden die een get request doet naar de api.
Met die get request wordt alle tekst over de onderdelen van het rapport binnen gehaald die dan met de 
ReadStock class in de stockReader map verwerkt wordt en alles dat in de request gemapt wordt aan de juiste class in de domain folder.
In stockReader wordt dan ook als alle data is verwerkt een map aangemaakt van de data die dan makkelijk te gebruiken is door ander methodes.

## Domain
De dataclasses voor het verwerken van de get request van de api.

## Tools
Onder tools hebben we een aantal classes die vooral als gereedschap gebruikt worden om data te verwerken.

### Screenshot
In de screenshot map bevinding zich 3 classes BaseScreenshotTaker, ScreenshotTaker en ScreenshotsRapport.
#### BaseScreenshotTaker
Deze class is de hoofdclass voor het nemen van de screenshots van een webpaginan hier wordt de Selenium library gebruikt om een headless firefox te draaien
met de nodige cookies (nodig voor de reclame weg te werken) en items in localstorage (nodig voor de "accept cookies" box weg te werken).
#### ScreenshotTaker
ScreenshotTaker extends BaseScreenshotTaker en voert de bewerking uit om de screenshots te maken van de elementen op een pagina die via hun Xpath worden gezocht.
#### ScreenshotsRapport
Hier wordt dan de ScreenshotTaker gebruikt om de nodige screenshots te maken van de pagina en ze dan ook op te slaan in de juist map.

### Tts
Hier bevindt zich het text to speech gedeelte, de Polly class is het deel die de verwerking doet van de tekst en communiceerd met de text to speech service.
In de SpeechGenerator wordt de Polly class dan aangesproken om dan de tekst die is binnengehaald door de ReadStock class te veranderen in geluid.
Voor dit deel kan werken moeten eerst de stappen uitgelegd in [Text to Speech](#text-to-speech) gevolgd worden.

### Videobuilder
In de VideoBuilder class wordt via de ProcessBuilder in java een commando via bash uitgevoerd die het Remotion project dan een video laat builden met de gemaakte json file.

### DirectorySorter
Deze class maakt het mogelijk om de files die beschikbaar zijn gemaakt voor de video op te halen en dan samen met de tekst data die komt van de api fragmenten te maken die gemaakt zijn uit de videoComponents

### TimelineBuilder
Met de TimelineBuilder wordt er een json opgesteld uit de gemaakte fragmenten van de DirectorySorter die dan gelezen kan worden door de videobuilder.

### Writer
Hier kunnen alle methodes teruggevonden worden die nodig zijn voor het schrijven van de gemaakte json naar de directory waar het verwacht wordt.

### Config
Hier wordt de configuratie ingeladen vanuit de default-config file onder resources. Hiervan wordt bij de eerste keer starten een kopie gemaakt naar ./config.

### VideoComponents
Een fragment is een stuk dat in de video voorkomt. Dit kan ofwel een Frame (picture) met audio zijn, ofwel een slide (text) zijn.

# Auteurs

- Lennert Commeine
- Wannes Matthys
