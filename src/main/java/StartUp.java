import tools.TimelineBuilder;
import tools.tts.SpeechGenerator;
import tools.screenshot.ScreenshotsRapport;
import tools.videoBuilder.VideoBuilder;


public class StartUp {
    private final static Boolean WITH_TTS = false;

    public static void main(String[] args) throws InterruptedException {
        if (WITH_TTS) {
            SpeechGenerator speechGenerator = new SpeechGenerator();
            speechGenerator.generateSpeech();
        }

        ScreenshotsRapport screenshotsRapport = new ScreenshotsRapport();
        screenshotsRapport.generateScreens();

        TimelineBuilder timelineBuilder = new TimelineBuilder();
        timelineBuilder.makeTimelineFiles();

        VideoBuilder videoBuilder = new VideoBuilder();
        videoBuilder.createVideo();
    }
}
