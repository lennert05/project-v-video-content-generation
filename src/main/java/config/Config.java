package config;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;



public class Config {
    private static final String JSON_CONFIG_FILE = "./config.json";
    private static JsonObject config;

    public JsonObject getConfig() {
        if (config == null) {
            readConfigFile();
        }
        return config;
    }

    private void readConfigFile() {
        if (!new File(JSON_CONFIG_FILE).exists()){
            try {
                createConfigFile();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        try {
            Object obj =JsonParser.parseReader(new FileReader(JSON_CONFIG_FILE));
            config = (JsonObject) obj;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void createConfigFile() throws IOException {
        InputStream inputStream = getClass().getResourceAsStream("/default-config.json");
        File dest = new File("./config.json");
        assert inputStream != null;
        FileUtils.copyInputStreamToFile(inputStream, dest);
    }

}
