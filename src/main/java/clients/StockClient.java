package clients;

import config.Config;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;
import org.checkerframework.checker.units.qual.C;

public class StockClient {

    private final Vertx vertx = Vertx.vertx();
    private final WebClient webClient = WebClient.create(vertx);

    public Future<String> getStock() {
        Config config = new Config();
        return webClient.get(config.getConfig().get("port").getAsInt(),
                        config.getConfig().get("apiHost").getAsString(),
                        config.getConfig().get("requestURI").getAsString())
                .ssl(true)
                .putHeader("Cookie", "viewAmount=3; _ga=GA1.2.90854192.1636536774; _gid=GA1.2.477550501.1636536774; _gat=1; __gads=ID=a30e293e3b64fd8f-22ddcccc40cb00ce:T=1636536775:RT=1636536775:S=ALNI_MZ_ezZRcpFjaCafvN7Tl2oFgSpAow; sessionId=f1375c1c2dfa41e94dab6f7aec15db044e633be6b9ddb7f951cc5414bc01ee18; o=198157")
                .as(BodyCodec.string())
                .send()
                .map(HttpResponse::body)
                .onSuccess(response -> vertx.close())
                .onFailure(response -> vertx.close());
    }
}
