package domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class HealthRating extends Rating {

    @JsonCreator
    public HealthRating(@JsonProperty("score") int score, @JsonProperty("positive") ArrayList<String> positive, @JsonProperty("neutral") ArrayList<String> neutral, @JsonProperty("negative") ArrayList<String> negative) {
        super(score, positive, neutral, negative);
    }

    public String getDeptToEquity() {
        return explanations.stream()
                .filter(e -> e.contains("Debt to Equity"))
                .findFirst()
                .orElse("");
    }

    public String getQuickRatio() {
        return explanations.stream()
                .filter(e -> e.contains("Quick Ratio")).map(Object::toString)
                .collect(Collectors.joining(" "));
    }

    public String getCurrentRatio() {
        return explanations.stream()
                .filter(e -> e.contains("Current Ratio")).map(Object::toString)
                .collect(Collectors.joining(" "));
    }

    public String getAltmanZ() {
        return explanations.stream()
                .filter(e -> e.contains("Altman-Z"))
                .findFirst()
                .orElse("");
    }

    public String tts() {
        return getDeptToEquity() + getQuickRatio() + getCurrentRatio() + getAltmanZ();
    }
}
