package domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Stock {
    private String summary;
    private ProfitabilityRating profitability;
    private ValuationRating valuation;
    private GrowthRating growth;
    private HealthRating health;
    private DividendRating dividend;

    public Stock() {
    }

    private void setSummary(String summary) {
        this.summary = summary.replaceAll("<strong>|</strong>", "");
    }

    public ProfitabilityRating getProfitability() {
        return profitability;
    }

    private void setProfitability(ProfitabilityRating profitability) {
        this.profitability = profitability;
    }

    public ValuationRating getValuation() {
        return valuation;
    }

    private void setValuation(ValuationRating valuation) {
        this.valuation = valuation;
    }

    public GrowthRating getGrowth() {
        return growth;
    }

    private void setGrowth(GrowthRating growth) {
        this.growth = growth;
    }

    public HealthRating getHealth() {
        return health;
    }

    private void setHealth(HealthRating health) {
        this.health = health;
    }

    public DividendRating getDividend() {
        return dividend;
    }

    private void setDividend(DividendRating dividend) {
        this.dividend = dividend;
    }

    public String getSummary() {
        return summary;
    }
}
