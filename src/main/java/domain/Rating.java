package domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Rating {

    protected final int score;
    protected final ArrayList<String> explanations = new ArrayList<>();

    @JsonCreator
    public Rating(@JsonProperty("score") int score, @JsonProperty("positive") ArrayList<String> positive, @JsonProperty("neutral") ArrayList<String> neutral, @JsonProperty("negative") ArrayList<String> negative) {
        this.score = score;
        explanations.addAll(positive.stream().map(sentence -> sentence.replaceAll("<strong>|</strong>", "")).toList());
        explanations.addAll(neutral.stream().map(sentence -> sentence.replaceAll("<strong>|</strong>", "")).toList());
        explanations.addAll(negative.stream().map(sentence -> sentence.replaceAll("<strong>|</strong>", "")).toList());
    }

    public int getScore() {
        return score;
    }

    public ArrayList<String> getExplanations() {
        return explanations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rating rating = (Rating) o;
        return score == rating.score && Objects.equals(explanations, rating.explanations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(score, explanations);
    }
}
