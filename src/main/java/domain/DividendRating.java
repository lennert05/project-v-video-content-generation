
package domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class DividendRating extends Rating {

    @JsonCreator
    public DividendRating(@JsonProperty("score") int score, @JsonProperty("positive") ArrayList<String> positive, @JsonProperty("neutral") ArrayList<String> neutral, @JsonProperty("negative") ArrayList<String> negative) {
        super(score, positive, neutral, negative);
    }

    public boolean hasDividend() {
        return explanations.size() > 1;
    }

    public String tts() {
        return explanations.stream()
                .collect(Collectors.joining(" "));
    }

}
