package domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class ProfitabilityRating extends Rating {

    @JsonCreator
    public ProfitabilityRating(@JsonProperty("score") int score, @JsonProperty("positive") ArrayList<String> positive, @JsonProperty("neutral") ArrayList<String> neutral, @JsonProperty("negative") ArrayList<String> negative) {
        super(score, positive, neutral, negative);
    }

    public String getReturnOnAssets() {
        return explanations.stream()
                .filter(e -> e.contains("Return On Assets"))
                .findFirst()
                .orElse("");
    }

    public String getProfitMargin() {
        return explanations.stream()
                .filter(e -> e.contains("Profit Margin"))
                .findFirst()
                .orElse("");
    }

    public String getReturnOnEquity() {
        return explanations.stream()
                .filter(e -> e.contains("Return On Equity"))
                .findFirst()
                .orElse("");
    }

    public String tts() {
        return getReturnOnAssets() + getReturnOnEquity() + getProfitMargin();
    }

    public String PiotroskiFScore() {
        return explanations.stream()
                .filter(e -> e.contains("Piotroski-F score"))
                .findFirst()
                .orElse("");
    }

}
