
package domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class GrowthRating extends Rating {

    @JsonCreator
    public GrowthRating(@JsonProperty("score") int score, @JsonProperty("positive") ArrayList<String> positive, @JsonProperty("neutral") ArrayList<String> neutral, @JsonProperty("negative") ArrayList<String> negative) {
        super(score, positive, neutral, negative);
    }

    public String getEarningsPerShare() {
        return explanations.stream()
                .filter(e -> e.contains("Earnings Per Share")).map(Object::toString)
                .collect(Collectors.joining(" "));
    }

    public String getRevenues() {
        return explanations.stream()
                .filter(e -> e.contains("Revenue")).map(Object::toString)
                .collect(Collectors.joining(" "));
    }

    public String tts() {
        return getEarningsPerShare() + getRevenues();
    }

}
