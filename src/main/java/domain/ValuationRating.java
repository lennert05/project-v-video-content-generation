package domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class ValuationRating extends Rating {

    @JsonCreator
    public ValuationRating(@JsonProperty("score") int score, @JsonProperty("positive") ArrayList<String> positive, @JsonProperty("neutral") ArrayList<String> neutral, @JsonProperty("negative") ArrayList<String> negative) {
        super(score, positive, neutral, negative);
    }

    public String getPriceToEarnings() {
        return explanations.stream()
                .filter(e -> e.contains("Price/Earning")).map(Object::toString)
                .collect(Collectors.joining(" "));
    }

    public String getPriceToBook() {
        return explanations.stream()
                .filter(e -> e.contains("book")).map(Object::toString)
                .collect(Collectors.joining(" "));
    }

    public String getEnterpriseValueToEBITDA() {
        return explanations.stream()
                .filter(e -> e.contains("Enterprise Value to EBITDA"))
                .findFirst()
                .orElse("");
    }

    public String tts() {
        return getPriceToEarnings() + getPriceToBook() + getEnterpriseValueToEBITDA();
    }
}
