package tools;

import config.Config;
import tools.stockReader.ReadStock;
import videoComponents.Fragment;
import videoComponents.Frame;
import videoComponents.Track;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DirectorySorter {
    private final Config config = new Config();

    private List<String> findFiles(Path path, String[] fileExtensions) {
        if (!Files.isDirectory(path)) {
            throw new IllegalArgumentException("Path must be a directory!");
        }
        List<String> result = null;
        try (Stream<Path> walk = Files.walk(path)) {
            result = walk
                    .filter(p -> !Files.isDirectory(p))
                    .map(Path::toString)
                    .filter(f -> Arrays.stream(fileExtensions).anyMatch(f::endsWith))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Fragment> getFragments(){
        List<String>imageList = findFiles(Path.of(config.getConfig().get("imageFolder").getAsString()), new String[]{"jpg", "jpeg", "png"}) ;
        List<String>audioList = findFiles(Path.of(config.getConfig().get("audioFolder").getAsString()), new String[]{"wav", "mp3"});
        ReadStock readStock = new ReadStock();
        List<Frame> frames = createFrameList(imageList);
        List<Track> tracks = createTrackList(audioList);
        Map<String, String> text = readStock.stockToList();
        return createFragmentList(frames, tracks, text);

    }

    public List<Fragment> createFragmentList(List<Frame> imageList, List<Track> audioList, Map<String, String> text){
        List<Fragment> fragmentList = new ArrayList<>();
        AtomicInteger i = new AtomicInteger(0);
        text.forEach((k, v)->{
            int index = i.getAndIncrement();
            fragmentList.add(new Fragment(k, "SLIDE"));
            fragmentList.add(new Fragment(imageList.get(index), audioList.get(index), v, "IMAGE_AUDIO"));

        });
        return fragmentList;
    }

    public List<Track> createTrackList(List<String> fileList){
        List<Track> tracks = new ArrayList<>();
        Collections.sort(fileList);
        for (String file: fileList ) {
            Track track = new Track(Paths.get(file));
            tracks.add(track);
        }
        return tracks;
    }

    public List<Frame> createFrameList(List<String> fileList){
        List<Frame> frames = new ArrayList<>();
        Collections.sort(fileList);
        for (String file: fileList ) {
            Frame frame = new Frame(Paths.get(file));
            frames.add(frame);
        }
        return frames;
    }
}
