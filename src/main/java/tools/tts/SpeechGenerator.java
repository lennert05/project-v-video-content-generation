package tools.tts;

import domain.Stock;
import tools.stockReader.ReadStock;

public class SpeechGenerator {

    public void generateSpeech() {
        ReadStock stockReader = new ReadStock();
        Stock stock = stockReader.fetchData();

        Polly.synthesizeSpeech(stock.getSummary(), "0001.mp3");
        Polly.synthesizeSpeech(stock.getProfitability().tts(), "0002.mp3");
        Polly.synthesizeSpeech(stock.getValuation().tts(), "0003.mp3");
        Polly.synthesizeSpeech(stock.getGrowth().tts(), "0004.mp3");
        Polly.synthesizeSpeech(stock.getHealth().tts(), "0005.mp3");
        Polly.synthesizeSpeech(stock.getDividend().tts(), "0006.mp3");
    }

}
