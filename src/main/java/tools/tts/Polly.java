package tools.tts;

import com.amazonaws.services.polly.AmazonPolly;
import com.amazonaws.services.polly.AmazonPollyClientBuilder;
import com.amazonaws.services.polly.model.OutputFormat;
import com.amazonaws.services.polly.model.SynthesizeSpeechRequest;
import com.amazonaws.services.polly.model.SynthesizeSpeechResult;
import com.amazonaws.services.polly.model.VoiceId;
import config.Config;

import java.io.FileOutputStream;
import java.io.InputStream;

public class Polly {
    private static AmazonPolly client = AmazonPollyClientBuilder.defaultClient();
    private static Config config = new Config();

    private Polly() {

    }

    public static void synthesizeSpeech(String text, String outputFileName) {
        SynthesizeSpeechRequest synthesizeSpeechRequest = new SynthesizeSpeechRequest()
                .withOutputFormat(OutputFormat.Mp3)
                .withVoiceId(VoiceId.Joanna)
                .withText(text)
                .withEngine("neural");

        String path = config.getConfig().get("audioFolder").getAsString();
        try (FileOutputStream outputStream = new FileOutputStream(path + "/" + outputFileName)) {
            SynthesizeSpeechResult synthesizeSpeechResult = client.synthesizeSpeech(synthesizeSpeechRequest);
            byte[] buffer = new byte[2 * 1024];
            int readBytes;

            try (InputStream in = synthesizeSpeechResult.getAudioStream()){
                while ((readBytes = in.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, readBytes);
                }
            }
        } catch (Exception e) {
            System.err.println("Exception caught: " + e);
        }
    }
}