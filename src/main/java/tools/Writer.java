package tools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;

public class Writer {

    private Path createNewFile(Path directory, String name) {
        File myObj = new File(String.format("%s/%s.json", directory, name));
        try {
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return myObj.toPath();
    }

    public void writeToFile(Path path, String content, String filename) {
        Path filePath = createNewFile(path, filename);
        try {
            FileWriter myWriter = new FileWriter(String.valueOf(filePath));
            myWriter.write(content);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
