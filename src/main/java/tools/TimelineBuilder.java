package tools;

import config.Config;
import org.json.JSONArray;
import org.json.JSONObject;
import videoComponents.Fragment;
import java.nio.file.Path;
import java.util.List;


public class TimelineBuilder {
    private final Config config = new Config();
    Writer writer = new Writer();

    public void makeTimelineFiles() {
        DirectorySorter sorter = new DirectorySorter();
        List<Fragment> parts = sorter.getFragments();
        writer.writeToFile(Path.of(config.getConfig().get("jsVideoBuilderFolder").getAsString()), makeJsonInput(parts), "input");
    }

    public String makeJsonInput(List<Fragment> fragments){
        JSONArray ja = new JSONArray();
        fragments.forEach(fragment -> ja.put(new JSONObject()
                .put("id", Integer.toString(fragments.indexOf(fragment)))
                .put("image", fragment.getFrame() != null ? fragment.getFrame().getPath().toString()
                        .replace(config.getConfig().get("workFolder").getAsString() + "/", "") : JSONObject.NULL)
                .put("audio", fragment.getTrack() != null ? fragment.getTrack().getPath().toString()
                        .replace(config.getConfig().get("workFolder").getAsString() + "/", "") : JSONObject.NULL)
                .put("text", fragment.getText())
                .put("type", fragment.getType())
                .put("durationInSeconds", fragment.getTrack() != null ? fragment.getTrack().getDuration() : 3)
        ));
        return ja.toString();
    }

}
