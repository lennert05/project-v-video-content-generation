package tools.videoBuilder;


import config.Config;

import java.io.File;
import java.io.IOException;

public class VideoBuilder {

    private final Config config = new Config();

    public void createVideo() {
        try {
            ProcessBuilder builder = new ProcessBuilder("bash", "-c", String.format("yarn build"));

            builder.directory(new File(config.getConfig().get("jsVideoBuilderFolder").getAsString()));
            Process process = builder.start();
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
