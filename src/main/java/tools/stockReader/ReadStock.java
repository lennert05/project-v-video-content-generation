package tools.stockReader;

import clients.StockClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.Stock;
import io.vertx.core.Future;

import java.util.LinkedHashMap;
import java.util.Map;


public class ReadStock {
    public Stock fetchData() {
        boolean process = false;
        StockClient stockClient = new StockClient();
        Future<String> future = stockClient.getStock();
        //process synchronous maken (niet de beste manier maar werkt voor ons concept)
        while (!process){
            process = future.isComplete();
        }
        return succeeded(future.result());
    }

    private Stock succeeded(String stockAsString) {
        Stock stock = new Stock();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            stock = objectMapper.readValue(stockAsString, Stock.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stock;

    }

    public Map<String, String> stockToList(){
        Stock stock = fetchData();

        Map<String, String> mapOfStocks = new LinkedHashMap<>();
        mapOfStocks.put("Fundamental Rating", stock.getSummary());
        mapOfStocks.put("Profitability Rating", stock.getProfitability().getExplanations().toString());
        mapOfStocks.put("Valuation Rating", stock.getValuation().getExplanations().toString());
        mapOfStocks.put("Growth Rating", stock.getGrowth().getExplanations().toString());
        mapOfStocks.put("Health Rating", stock.getHealth().getExplanations().toString());
        mapOfStocks.put("Dividend Rating", stock.getDividend().getExplanations().toString());

        return mapOfStocks;
    }
}
