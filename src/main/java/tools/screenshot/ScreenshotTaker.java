//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package tools.screenshot;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.io.IOException;

public class ScreenshotTaker extends tools.screenshot.BaseScreenshotTaker {
    public ScreenshotTaker(String link) throws InterruptedException {

        this.setUrl(link);
        this.setLocalStorage();
        this.setCookies();

        Thread.sleep(2000L);
    }

    public void byXPath(String xpath, String filename) {
        try {
            WebElement test = this.driver.findElement(By.xpath(xpath));
            File f = test.getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(f, new File(String.format("%s/%s.png", this.config.get("imageFolder").getAsString(), filename)));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
