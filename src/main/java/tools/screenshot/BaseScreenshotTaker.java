

package tools.screenshot;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import config.Config;

import java.io.*;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;


abstract class BaseScreenshotTaker {
    private final Config configuration = new Config();
    public final WebDriver driver;
    protected final JsonObject config = configuration.getConfig();

    public BaseScreenshotTaker() {
        this.setDriver();
        FirefoxOptions options = this.getOptions();
        this.driver = new FirefoxDriver(options);
        this.setSize();
    }

    private void setDriver() {
        if (!new File("./geckodriver").exists()){
            try {
                createDriverFile();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        System.out.println();

        System.setProperty(
                this.config.get("driverName").getAsString(),
                "."+this.config.get("driverPath").getAsString()
        );
    }

    private void setSize() {
        this.driver.manage().window().setSize(new Dimension(1920, 1080));
    }

    protected void setCookies() {
        HashMap<String, String> cookies = (new Gson()).fromJson(this.config.getAsJsonObject("cookies"), HashMap.class);
        cookies.forEach((k, v)->{
            this.driver.manage().addCookie(new Cookie(k, v));
        });

    }

    protected void setLocalStorage(){
        HashMap<String, String> storageItems = (new Gson()).fromJson(this.config.getAsJsonObject("localStorage"), HashMap.class);
        LocalStorage localStorage = ((WebStorage) driver).getLocalStorage();
        storageItems.forEach(localStorage::setItem);
        driver.navigate().refresh();
    }

    protected void setUrl(String url) {
        this.driver.get(url);
    }

    private FirefoxOptions getOptions() {
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments(new String[]{"--headless"});
        return options;
    }

    private void createDriverFile() throws IOException {
        InputStream inputStream = getClass().getResourceAsStream(config.get("driverPath").getAsString());
        File dest = new File("."+config.get("driverPath").getAsString());
        assert inputStream != null;
        FileUtils.copyInputStreamToFile(inputStream, dest);
        dest.setExecutable(true);
        dest.setReadable(true);
        System.out.println(dest.canExecute());
    }

    public void quitDriver() {
        this.driver.quit();
    }
}
