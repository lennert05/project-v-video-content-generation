package tools.screenshot;

import com.google.gson.Gson;
import config.Config;

import java.util.LinkedHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ScreenshotsRapport {
    Config config = new Config();

    public void generateScreens() throws InterruptedException {
        ScreenshotTaker screenshotTaker = new ScreenshotTaker(config.getConfig().get("host").getAsString());
        LinkedHashMap<String, String> xpathLocations = new Gson().fromJson(config.getConfig().getAsJsonObject("xpathLocations"), LinkedHashMap.class);
        AtomicInteger counter = new AtomicInteger(0);
        xpathLocations.forEach((name, xpath)->{
            screenshotTaker.byXPath(xpath, String.format("%dscreen", counter.getAndIncrement()));
        });

    }
}
