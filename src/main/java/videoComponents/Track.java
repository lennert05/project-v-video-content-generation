package videoComponents;


import com.github.kokorin.jaffree.ffmpeg.*;

import java.nio.file.Path;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;


public class Track extends Component {
    public Track(Path path) {
        super(path);
    }

    public long getDuration() {
        final AtomicLong duration = new AtomicLong();

        FFmpeg.atPath()
                .addInput(UrlInput.fromPath(this.getPath()))
                .addOutput(new NullOutput())
                .setProgressListener(progress -> duration.set(progress.getTime(TimeUnit.SECONDS)))
                .execute();
        //anders plakt het meeste te veel aan elkaar
        return duration.get() + 1;

    }
}
