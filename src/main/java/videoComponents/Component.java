package videoComponents;

import java.nio.file.Path;

public class Component {
    private final Path path;


    public Path getPath() {
        return path;
    }

    public Component(Path path) {
        this.path = path;
    }
}
