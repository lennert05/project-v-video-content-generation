package videoComponents;


public class Fragment {
    private final Frame frame;
    private final Track track;
    private final String text;
    private final String type;

    public Fragment(Frame picture, Track audio, String text, String type) {
        this.frame = picture;
        this.track = audio;
        this.text = text;
        this.type = type;

    }

    public Fragment(String text, String type) {
        this(null, null, text, type);
    }

    public Frame getFrame() {
        return frame;
    }

    public Track getTrack() {
        return track;
    }

    public String getText(){ return text;}

    public String getType(){ return type;}


}
